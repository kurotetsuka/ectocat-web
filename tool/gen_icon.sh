#!/bin/bash

sizes="16 24 32 64 128 256"

mkdir -p gen/img

for size in $sizes; do
	sizestr="${size}x${size}"
	gm convert res/ectocat.png \
		-resize ${sizestr} \
		gen/img/ectocat_${sizestr}.png
	echo "done ectocat_${sizestr}.png"
done
