<title>Project Ectocat</title>

<!-- ink framework meta shit -->
<meta charset="utf-8">
<meta name="author" content="kurotetsuka">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!-- ink framework css includes -->
<link rel="stylesheet" type="text/css" href="/ectocat/lib/ink/css/ink-flex.min.css">
<link rel="stylesheet" type="text/css" href="/ectocat/lib/ink/css/font-awesome.min.css">

<!-- ectocat css includes -->
<link href="/ectocat/css/main.css" rel="stylesheet">
<link rel="shortcut icon" href="/ectocat/img/ectocat_16x16.png">
<link rel="shortcut icon" href="/ectocat/img/ectocat_24x24.png">
<link rel="shortcut icon" href="/ectocat/img/ectocat_32x32.png">
<link rel="shortcut icon" href="/ectocat/img/ectocat_64x64.png">

<!-- ink framework js includes -->
<script type="text/javascript" src="/ectocat/lib/ink/js/modernizr.js"></script>
<script type="text/javascript">
	Modernizr.load({
		test: Modernizr.flexbox,
		nope : '/lib/ink/css/ink-legacy.min.css'});
</script>

<script type="text/javascript" src="/ectocat/lib/ink/js/holder.js"></script>
<script type="text/javascript" src="/ectocat/lib/ink/js/ink-all.min.js"></script>
<script type="text/javascript" src="/ectocat/lib/ink/js/autoload.js"></script>

<!-- ectocat js includes -->

