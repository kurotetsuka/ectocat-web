<!DOCTYPE html>
<html lang="en">
<head>
	<?php include $_SERVER['DOCUMENT_ROOT'].'/ectocat/include/root.php';?>
	<?php include $root.'/include/head.php';?>
	<style type="text/css">
		body {
			background: #ededed;}

		header h1 small {
			color: #4c4c4c;}
		header h1 small:before {
			content: "|";
			margin: 0 0.5em;
			font-size: 1.6em;}
		#ectocat_icon {
			width: 2.0em;
			vertical-align: middle;}
		#search-button {
			min-width: 5em;}

		/* search form stuff */
		@media all and (min-width: 960px) and (max-width: 1120px) {
			.tagline {
				display: none;}
		}
		@media all and (min-width: 0px) and (max-width: 720px) {
			.tagline {
				display: none;}
		}

		article header{
			padding: 0;
			overflow: hidden;}

		article footer {
			background: none;}

		article {
			padding-bottom: 2em;
			border-bottom: 1px solid #ccc;}

		.date {
			float: right;}

		summary {
			font-weight: 700;
			line-height: 1.5;}

		footer {
			background: #ccc;}
	</style>
</head>
<body>
	<!--[if lte IE 9 ]>
	<div class="ink-alert basic" role="alert">
		<button class="ink-dismiss">&times;</button>
		<p>
			<strong>Get a better browser, son.</strong>
			( <a href="http://browsehappy.com/">upgrade to a modern  browser</a> to improve your web experience, and make this page look and feel half-decent. )
		</p>
	</div>
	<![endif]-->
	<!-- page content start -->
	<div class="ink-grid">

		<header class="clearfix vertical-padding">
			<h1 class="logo xlarge-push-left large-push-left">
				<a href="/ectocat/index.php">
					<img id="ectocat_icon" src="/ectocat/img/ectocat_128x128.png"/></a>
				Project Ectocat <small class="tagline hide-tiny hide-small">Open Source Health Analytics</small>
			</h1>
			<!--<nav class="ink-navigation xlarge-push-right large-push-right half-top-space">
				<ul class="menu horizontal black">
					<li class="active"><a href="#">item</a></li>
					<li><a href="#">item</a></li>
					<li><a href="#">item</a></li>
				</ul>
			</nav>-->
			<form class="ink-form xlarge-push-right large-push-right half-top-space">
				<div class="control-group column-group all-push-center">
					<div class="control prepend-symbol medium-80 small-70 tiny-60">
						<span>
							<i class="fa fa-search"></i>
							<input class="dark" type="text" name="query" placeholder="Project Name">
						</span>
					</div>
					<div class="control">
						<input type="submit" class="ink-button black" id="search-button">
					</div>
				</div>
			</form>
		</header>

		<section class="column-group gutters article">
			<section class="xlarge-20 large-20 medium-30 small-100 tiny-100">
				<h2>Navigation</h2>
				<nav class="ink-navigation">
					<ul class="menu vertical black">
						<li class="heading active"><a href="#">Overview</a></li>
						<li class=""><a href="#">Commits</a></li>
						<li class=""><a href="#">Issues</a></li>
						<li class="disabled"><a href="#">Reddit</a></li>
						<li class="disabled"><a href="#">Mailing Lists</a></li>
					</ul>
				</nav>
				<!--<h2>Related</h2>
				<ul class="unstyled">
					<li class="column-group half-gutters">
					<button class="ink-button">Overview</button>
					</li>
					<li class="column-group half-gutters">
					<button class="ink-button">Commits</button>
					</li>
					<li class="column-group half-gutters">
					<button class="ink-button">Issues</button>
					</li>
					<li class="column-group half-gutters">
					<button class="ink-button" disabled>Reddit</button>
					</li>
					<li class="column-group half-gutters">
					<button class="ink-button" disabled>Mailing Lists</button>
					</li>
				</ul>-->
			</section>
			<div class="xlarge-80 large-80 medium-70 small-100 tiny-100">
				<article>
					<header>
						<h1 class="push-left">Project Overview</h1> 
						<p class="push-right">Last Update: <time pubdate="pubdate">2014-10-27</time></p>
					</header>
					<summary>But being in a great hurry to resume scolding the man in the purple Shirt, who was waiting for it in the entry, and seeming to hear nothing but the word "clam," Mrs. Hussey hurried towards an open door leading to the kitchen, and bawling out "clam for two," disappeared.</summary>
					
					<p>However, a warm savory steam from the kitchen served to belie the apparently cheerless prospect before us. But when that smoking chowder came in, the mystery was delightfully explained. Oh, sweet friends! hearken to me. It was made of small juicy clams, scarcely bigger than hazel nuts, mixed with pounded ship biscuit, and salted pork cut up into little flakes; the whole enriched with butter, and plentifully seasoned with pepper and salt. Our appetites being sharpened by the frosty voyage, and in particular, Queequeg seeing his favourite fishing food before him, and the chowder being surpassingly excellent, we despatched it with great expedition: when leaning back a moment and bethinking me of Mrs. Hussey's clam and cod announcement, I thought I would try a little experiment. Stepping to the kitchen door, I uttered the word "cod" with great emphasis, and resumed my seat. In a few moments the savoury steam came forth again, but with a different flavor, and in good time a fine cod-chowder was placed before us.</p>
					<footer>
						<p><small>Creative Commons Attribution-ShareAlike License</small></p>
					</footer>
				</article>
			</div>
		</section>
	</div>
	<footer class="clearfix">
		<div class="ink-grid">
			<ul class="unstyled inline half-vertical-space">
				<li class="active"><a href="#">About</a></li>
				<li><a href="#">Sitemap</a></li>
				<li><a href="#">Contacts</a></li>
			</ul>
			<p class="note">&copy; <a href="https://github.com/kurotetsuka">kurotetsuka</a> 2014</p>
		</div>
	</footer>
	<!-- page content end -->
</body>
</html>