<!DOCTYPE html>
<html lang="en">
<head>
	<?php include $_SERVER['DOCUMENT_ROOT'].'/ectocat/include/root.php';?>
	<?php include $root.'/include/head.php';?>
	<style type="text/css">
		html, body {
			height: 100%;
			background: #ededed;}

		header h1 small:before  {
			content: "|";
			margin: 0 0.5em;
			font-size: 1.6em;}

		#ectocat_icon {
			width: 8em;}
		#search-button {
			min-width: 5em;}

		.wrap {
			min-height: 100%;
			height: auto !important;
			height: 100%;
			margin: 0 auto -120px;
			overflow: auto;}

		.push, footer {
			height: 120px;
			margin-top: 0;}

		footer {
			background: #ccc;
			border: 0;}
		footer * {
			line-height: inherit;}
	</style>
</head>
<body>
	<!-- page content start -->
	<!--[if lte IE 9 ]>
	<div class="ink-alert basic" role="alert">
		<button class="ink-dismiss">&times;</button>
		<p>
			<strong>Get a better browser, son.</strong>
			( <a href="http://browsehappy.com/">upgrade to a modern  browser</a> to improve your web experience, and make this page look half-decent. )
		</p>
	</div>
	<![endif]-->

	<!--<div class="wrap">
		<div class="ink-grid vertical-space">
			<a href="/index.php">
				<img id="ectocat_icon" src="/ectocat/img/ectocat.svg"/></a>
			<h1>Project Ectocat</h1>
			Open Source Health Analytics
		</div>
		<div class="push"></div>
	</div>

	<footer class="clearfix">
		<div class="ink-grid">
			<ul class="unstyled inline half-vertical-space">
				<li class="active"><a href="#">About</a></li>
				<li><a href="#">Sitemap</a></li>
				<li><a href="#">Contacts</a></li>
			</ul>
			<p class="note">&copy; <a href="https://github.com/kurotetsuka">kurotetsuka</a> 2014</p>
		</div>
	</footer>-->
	<div class="wrap">
		<div class="ink-grid vertical-space">
			<a href="/index.php">
				<img id="ectocat_icon" src="/ectocat/img/ectocat.svg"/></a>
			<h1>Project Ectocat</h1>
			<p>Open Source Health Analytics</p>
			<form class="ink-form xlarge-push-right large-push-right half-top-space">
				<div class="control-group column-group">
					<div class="control prepend-symbol xlarge-80 large-80 medium-80 small-70 tiny-70">
						<span>
							<i class="fa fa-search"></i>
							<input class="dark" type="text" name="query" placeholder="Project Name">
						</span>
					</div>
					<div class="control">
						<input type="submit" class="ink-button black" id="search-button" value="Search">
					</div>
				</div>
			</form>
		</div>
		<div class="push"></div>
	</div>

	<footer class="clearfix">
		<div class="ink-grid">
			<ul class="unstyled inline half-vertical-space">
				<li class="active"><a href="#">About</a></li>
				<li><a href="#">Sitemap</a></li>
				<li><a href="#">Contacts</a></li>
			</ul>
			<p class="note">&copy; <a href="https://github.com/kurotetsuka">kurotetsuka</a> 2014</p>
		</div>
	</footer>
	<!-- page content end -->
</body>
</html>