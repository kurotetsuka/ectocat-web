## project ectocat :: open source health information ##

By: [kurotetsuka](github.com/kurotetsuka)  
This work is released under the GNU GPL. See [license.md](license.md) and [gnu-gpl-v3.0.md](legal/gnu-gpl-v3.0.md) for details. )